import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Handler:
	def saltlake_quit(self, *args):
		print("Stopped saltlake.")
		Gtk.main_quit()

	#def onButtonPressed(self, button):
	#	print("Hello World!")


builder = Gtk.Builder()
builder.add_from_file("saltlake_xml.glade")
builder.connect_signals(Handler())

#for obj in builder.get_objects():
#	print(Gtk.Buildable.get_name(obj))

window = builder.get_object("MainWindow")
window.set_name("Saltlake (flatpak frontend)")

PackageList = Gtk.ListStore("")

#builder.get_object("PackageListView").add(Gtk.List)

window.show_all()

Gtk.main()
