import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class RadioButtonWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="RadioButton Demo")
        grid = Gtk.Grid()
        '''
        hbox = Gtk.Box(spacing=6)
        self.add(hbox)

        button1 = Gtk.RadioButton.new_with_label_from_widget(None, "Button 1")
        button1.connect("toggled", self.on_button_toggled, "1")
        hbox.pack_start(button1, False, False, 0)

        button2 = Gtk.RadioButton.new_with_label_from_widget(button1, "Button 2")
        button2.connect("toggled", self.on_button_toggled, "2")
        hbox.pack_start(button2, False, False, 0)

        button3 = Gtk.RadioButton.new_with_mnemonic_from_widget(button1, "B_utton 3")
        button3.connect("toggled", self.on_button_toggled, "3")
        hbox.pack_start(button3, False, False, 0)
        '''
        menubar = Gtk.MenuBar()
        menubar.set_hexpand(True)
        
        menuitem1 = Gtk.MenuItem.new_with_mnemonic(label="_A Menu Item")
        menumasterlist = [
            [],
            []
        ]
        
        for i in range(1, 4):
            mir = Gtk.RadioMenuItem.new_with_mnemonic(menumasterlist[0], "Radio _" + str(i))
            menumasterlist[0].append(mir)
            mir.set_active(False)
        
        menumasterlist[0][0].set_active(True)
        
        menumasterlist[0].append(Gtk.MenuItem.new_with_label("HEllo"))
        
        menuitem1.set_submenu(Gtk.Menu())
        for mir in menumasterlist[0]:
            menuitem1.get_submenu().add(mir)
        
        menubar.add(menuitem1)
        
        grid.attach(menubar, 0, 0, 1, 1)
        grid.attach(Gtk.Button(label="Hello"), 0, 1, 1, 1)
        self.add(grid)
        #'''
        
    def on_button_toggled(self, button, name):
        if button.get_active():
            state = "on"
        else:
            state = "off"
        print("Button", name, "was turned", state)


win = RadioButtonWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
