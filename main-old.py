import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gio
from gi.repository import Gtk

# Initialize window listbox for containing every other element
gtk_window_box = Gtk.Grid()



# Initialize menubar
menubar = Gtk.MenuBar()

menubar_items = [
    Gtk.MenuItem.new_with_mnemonic(label="_File"),  # File menu
    Gtk.MenuItem.new_with_mnemonic(label="_Edit"),  # Edit menu
    Gtk.MenuItem.new_with_mnemonic(label="_View"),  # View menu
    Gtk.MenuItem.new_with_mnemonic(label="_Help")   # Help menu
]

menubar_menu_items = [
    [
        Gtk.MenuItem.new_with_mnemonic(label="_Export installed packages to file..."),
        Gtk.MenuItem.new_with_mnemonic(label="_Install packages from file...")
    ],
    [
        Gtk.MenuItem.new_with_mnemonic(label="_Update list of packages"),
        Gtk.MenuItem.new_with_mnemonic(label="Update i_nstalled packages")
    ],
    [
        RadioMenuItemFix.new_with_mnemonic(None, "_Edit sources"),
        RadioMenuItemFix.new_with_mnemonic(None, "_Search package"),
        RadioMenuItemFix.new_with_mnemonic(None, "_Installed package")
       # Gtk.MenuItem.new_with_mnemonic(label="_Update installed packages")
    ],
    [
        Gtk.MenuItem.new_with_mnemonic(label="_Contents"),
        Gtk.MenuItem.new_with_mnemonic(label="_About")
    ]
]

for i in range(3):
    item = menubar_menu_items[2][i]
    item.set_group(menubar_menu_items[2])
    menubar_menu_items[2].append(item)
    item.set_active(False)

menubar_menu_items[2][1].set_active(True)


# Insert all menubar information to menubar
for i in range(len(menubar_items)):
    menuitem_menu = Gtk.Menu()
    
    for i2 in range(len(menubar_menu_items[i])):
        menuitem_menu.add(menubar_menu_items[i][i2])
    
    menubar_items[i].set_submenu(menuitem_menu)
    menubar.add(menubar_items[i])

'''
# File menu
menuitem_0 = Gtk.MenuItem.new_with_mnemonic(label="_File")
menuitem_0_menu = Gtk.Menu()

menuitem_0_0 = Gtk.MenuItem.new_with_mnemonic(label="_Export installed packages to file...")
menuitem_0_1 = Gtk.MenuItem.new_with_mnemonic(label="_Install packages from file...")

menuitem_0_menu.add(menuitem_0_0)
menuitem_0_menu.add(menuitem_0_1)

menuitem_0.set_submenu(menuitem_0_menu)
menubar.add(menuitem_0)


# Edit menu
menuitem_1 = Gtk.MenuItem.new_with_mnemonic(label="_Edit")
menuitem_1_menu = Gtk.Menu()

menuitem_1_0 = Gtk.MenuItem.new_with_mnemonic(label="_Update list of packages")
menuitem_1_1 = Gtk.MenuItem.new_with_mnemonic(label="Update i_nstalled packages")

group = []

menuitem_1_2 = Gtk.RadioMenuItem.new_with_mnemonic(group, "_Edit sources")
menuitem_1_3 = Gtk.RadioMenuItem.new_with_mnemonic(group, "_Search package")

group = menu_item.get_group()
self.menu_items[i] = menu_item
self.menu.append(menu_item)
menu_item.connect("activate", self.on_menu_select, i)
menu_item.show()


menuitem_1_menu.add(menuitem_1_0)
menuitem_1_menu.add(menuitem_1_1)
menuitem_1_menu.add(menuitem_1_2)
menuitem_1_menu.add(menuitem_1_3)

menuitem_1.set_submenu(menuitem_1_menu)
menubar.add(menuitem_1)


# View menu


# Help menu
menuitem_3 = Gtk.MenuItem.new_with_mnemonic(label="_Help")
menuitem_3_menu = Gtk.Menu()

menuitem_3_0 = Gtk.MenuItem.new_with_mnemonic(label="_Contents")
menuitem_3_1 = Gtk.MenuItem.new_with_mnemonic(label="_About")

menuitem_3_menu.add(menuitem_3_0)
menuitem_3_menu.add(menuitem_3_1)

menuitem_3.set_submenu(menuitem_3_menu)
menubar.add(menuitem_3)
'''

# Header
header = Gtk.HBox(spacing=6)
header.set_border_width(6)

def search_query(searchEntry):
    print("Running search for", searchEntry.get_text())

header_0 = Gtk.SearchEntry()
header_1 = Gtk.Button.new_with_mnemonic(label="_Search")

header_0.connect("activate", search_query)

header.pack_start(header_0, True, True, 0)
header.pack_start(header_1, False, False, 0)

header.set_hexpand(True)


# Content
content = Gtk.HBox()
content.set_vexpand(True)



# Initialize window
gtk_window = Gtk.Window(title="Salt Lake (flatpak frontend)")
gtk_window.connect("destroy", Gtk.main_quit)
gtk_window.set_default_size(720, 480)

# Add components to window box
gtk_window_box.attach(menubar, 0, 0, 1, 1)
gtk_window_box.attach(header,  0, 1, 1, 1)
gtk_window_box.attach(Gtk.Separator(), 0, 2, 1, 1)
gtk_window_box.attach(content, 0, 3, 1, 1)

# Add components to window
gtk_window.add(gtk_window_box)
gtk_window.show_all()

Gtk.main()
